
import './App.css';
import react from "./react.png";

function Header(props){
  console.log(props)
  return ( 
  <header>
    <img 
    src={react} 
    height={200} 
    alt="reactjs logo" 
    />
    <h1> {props.name} </h1>
    
  </header>
  );
}

function Main(props){
  return (
    
    <section>
      
      <h2>Why reactJS is {props.adjective} ? </h2>
      <ul>
        {props.subject.map((title) => <li key={title.id}> {title.title}</li>)}
      </ul>
    </section>
  )
}

function  Footer(props){
  return (
    <p style={{color:"red"}}>Copyright {props.year}</p>
  )
}

const subject_i_like = [
  "Declarative",
  "Component-based",
  "Learn Once, Write Anywhere"

];
const subjectObjects = subject_i_like.map((sub, i) => ({id: i, title: sub}));
subject_i_like.map((sub) => console.log(sub))

function App() {
  return (
    <div className="App">
  
      <Header name="Welcome to reactJS tutorial"/>
      <div className="list-view">
      <Main  adjective="super" subject={subjectObjects}/>
      </div>
      <Footer year={new Date().getFullYear()}/>

    </div>
  );
}

export default App;
